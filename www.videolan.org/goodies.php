<?php
   $title = "Goodies";
   $lang = "en";
   $menu = array( "project", "goodies" );
   $additional_css = array("/style/panels.css");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

        <h1 class="bigtitle" style='margin-bottom: 25px;'>VideoLAN Goodies</h1>
        <p>You are now a true VideoLAN fan? Help yourself in this page!</p>

        <div class="clearme" style='padding-top: 30px;'>
            <div style='float: left; width: 475px;'>
                <div class='audienceCallout'>For your computer</div>
                <div class="clearme">
                    <a href="#videos" class='noUnderline'>
                        <img src='//images.videolan.org/images/VLC-IconSmall.png' alt='VLC icon' class='smallProjectImg' />
                        <span class='productName'>Videos</span>
                        <span class='productDescription'>VLC promotion movies authored by community</span>
                    </a>
                </div>
                <div class="clearme" style='padding-top: 40px;'>
                    <a href="#cones" class='noUnderline'>
                        <img src='//images.videolan.org/images/VLC-IconSmall.png' alt='VLC icon' class='smallProjectImg' />
                        <span class='productName'>Cones</span>
                        <span class='productDescription'>The well known VideoLAN Cone, redesigned by community</span>
                    </a>
                </div>
                <div class="clearme" style='padding-top: 40px;'>
                    <a href="#wallpaper" class='noUnderline'>
                        <img src='//images.videolan.org/images/VLC-IconSmall.png' alt='VLC icon' class='smallProjectImg' />
                        <span class='productName'>Wallpaper</span>
                        <span class='productDescription'>Background images featuring VLC Media Player</span>
                    </a>
                </div>
                <div class="clearme" style='padding-top: 40px;'>
                    <a href="#boot" class='noUnderline'>
                        <img src='//images.videolan.org/images/VLC-IconSmall.png' alt='VLC icon' class='smallProjectImg' />
                        <span class='productName'>Boot Screens</span>
                        <span class='productDescription'>VLC Media Player from instant zero</span>
                    </a>
                </div>
                <div class="clearme" style='padding-top: 40px;'>
                    <a href="#banners" class='noUnderline'>
                        <img src='//images.videolan.org/images/VLC-IconSmall.png' alt='VLC icon' class='smallProjectImg' />
                        <span class='productName'>Banners</span>
                        <span class='productDescription'>Promote your favorite project through your website</span>
                    </a>
                </div>
            </div>

            <div style='float: left; padding-left: 30px; width: 475px;'>
                <div class='audienceCallout'>For YourSelf</div>
                <div class="clearme">
                    <a href="#clothing" class='noUnderline'>
                        <img src='//images.videolan.org/images/VLC-IconSmall.png' alt='VLC icon' class='smallProjectImg' />
                        <span class='productName'>Clothing</span>
                        <span class='productDescription'>Wear the famous cone</span>
                    </a>
                </div>
                <div class="clearme" style='padding-top: 40px;'>
                    <a href="#food" class='noUnderline'>
                        <img src='//images.videolan.org/images/VLC-IconSmall.png' alt='VLC icon' class='smallProjectImg' />
                        <span class='productName'>Food</span>
                        <span class='productDescription'>Eat the Cone ! Featuring VLC Chocolate</span>
                    </a>
                </div>
                <div class="clearme" style='padding-top: 40px;'>
                    <a href="#stationery" class='noUnderline'>
                        <img src='//images.videolan.org/images/VLC-IconSmall.png' alt='VLC icon' class='smallProjectImg' />
                        <span class='productName'>Stationery</span>
                        <span class='productDescription'>VLC Lights and stickers</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="clearme" style='padding-top: 30px;'></div>

        <a name="videos"></a>
        <h2>Videos</h2>

<p>Videos about vlc</p>

<ul>
<li>
<p>VLC promotion movie made by Adam Vian</p>
<a href="http://images.videolan.org/images/vlc-player.mp4">
<img src="//images.videolan.org/images/goodies/vlc-player-promo.png" alt="Adam Vian's movie" height="100"></a>
</li>
</ul>

        <div class="clearme" style='padding-top: 30px;'></div>

        <a name="cones"></a>
        <h2>Cones</h2>

<p>You may freely use these VideoLAN logos using the following
copyright:</p>

<pre>Copyright (c) 1996-2010 VideoLAN. This logo or a modified version
may be used or modified by anyone to refer to the VideoLAN project
or any product developed by the VideoLAN team, but does not indicate
endorsement by the project.</pre>

<p>There have been many many versions of the VideoLAN cone: </p>

<ul>

<li>
<a href="http://images.videolan.org/images/goodies/cone-26xPG.png">
<img src="//images.videolan.org/images/goodies/cone-26xPG.png" alt="made by Nirzar" width="300">
</a>
<p>made by <a href="http://twitter.com/nirzardp">Nirzar</a></p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/snowman.png">
<img src="//images.videolan.org/images/goodies/snowman.png" alt="made by Cory Simmons" width="300">
</a>
<p>made by <a href="http://mscns.com/">Cory Simmons</a></p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/cone-i2Es.png">
<img src="//images.videolan.org/images/goodies/cone-i2Es.png" alt="made by Mr. Lee" width="300">
</a>
<p>made by <a href="http://www.design-fu.co.uk/">Mr. Lee</a></p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/cone-buB2.png">
<img src="//images.videolan.org/images/goodies/cone-buB2.png" alt="made by Joel Shaw" width="300">
</a>
<p>made by Joel Shaw</p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/cone-eT5x.png">
<img src="//images.videolan.org/images/goodies/cone-eT5x.png" alt="made by Gautham aka Gapo" width="300">
</a>
<p>made by Gautham aka Gapo</p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/cone-raGi.png">
<img src="//images.videolan.org/images/goodies/cone-raGi.png" alt="made by Nick Pierno" width="300">
</a>
<a href="http://images.videolan.org/images/goodies/cone-RNF1.png">
<img src="//images.videolan.org/images/goodies/cone-RNF1.png" alt="made by Nick Pierno" width="300">
</a>
<p>made by <a href="http://www.topdraw.com/">Nick Pierno</a></p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/cone-gA4v.png">
<img src="//images.videolan.org/images/goodies/cone-gA4v.png" alt="made by SavTheCoder" width="300">
</a>
<p>made by SavTheCoder</p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/cone-FQCw.png">
<img src="//images.videolan.org/images/goodies/cone-FQCw.png" alt="made by digoncreative" width="300">
</a>
<p>made by digoncreative</p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/cone-8oPi.png">
<img src="//images.videolan.org/images/goodies/cone-8oPi.png" alt="made by TheBDutchman" width="300">
</a>
<p>made by TheBDutchman</p>
</li>

<li>
<a href="http://images.videolan.org/images/goodies/cone-f9my.png">
<img src="//images.videolan.org/images/goodies/cone-f9my.png" alt="made by mishnak" width="300">
</a>
<p>(joke) made by <a href="http://www.reddit.com/user/mishnak">mishnak</a></p>
</li>

<li>

<p>The version made by Stephane Soppera (with Blender):</p>
<a href="http://images.videolan.org/images/goodies/cone-soppera10.png">
<img src="//images.videolan.org/images/goodies/cone-soppera10-moyen.png" alt="Stephane Soppera's cone" height="100" width="77">
</a>

<p>Download the Blender sources <a href="http://images.videolan.org/images/goodies/cone10-soppera.blend">here</a>.</p>

</li>

<li>

<p>The first version of the logo made by Sam Hocevar (with The Gimp):</p>

<img src="//images.videolan.org/images/goodies/cone.png" alt="VideoLAN's first cone" height="100" width="78">

</li>
<li>

<p>Another version made by Benjamin Mironer that is still used in VLC
for Mac OS X:</p>

<img src="//images.videolan.org/images/goodies/cone-osx.png" alt="VideoLAN" height="127" width="115">

</li>
<li>

<p>A christmas icon made by Geoffrey Roussel:</p>
<p><img src="//images.videolan.org/images/goodies/CNoyel128.png" alt="Nowwwel"></p>
<p>Download the Blender sources <a href="http://images.videolan.org/images/goodies/vlcNoyel.blend">here</a>.</p>

</li>

<li>
<p>Two 3D cones made by Daniel Dreibrodt:</p>
<p>
<a href="http://d-gfx.kognetwork.ch/VLC/cone_altglass.png" target="_blank">
<img src="//images.videolan.org/images/goodies/cone_altglass_128.png" alt="Cone">
</a>

<a href="http://d-gfx.kognetwork.ch/VLC/cone_altglass_2.png" target="_blank">
<img src="//images.videolan.org/images/goodies/cone_altglass_2_128.png" alt="Cone">
</a>
</p>

<p>
<a href="http://d-gfx.kognetwork.ch/VLC/cone_altglass.ico">Download cone #1 as Windows XP/Vista icon</a><br>
<a href="http://d-gfx.kognetwork.ch/VLC/cone_altglass.blend">Download Blender sources of cone #1</a><br>
<a href="http://d-gfx.kognetwork.ch/VLC/cone_altglass_2.ico">Download cone #2 as Windows XP/Vista icon</a><br>
<a href="http://d-gfx.kognetwork.ch/VLC/cone_altglass_2.blend">Download Blender sources of cone #1</a><br>
</p>
</li>

<li>
<p>A nightly build cone based on the current icon made by Daniel Dreibrodt:</p>
<p>
<img src="//images.videolan.org/images/goodies/nightly_notext.png" alt="Nightly cone">
</p>
<p>
<a href="http://d-gfx.kognetwork.ch/VLC/nightly_notext.ico">Download it as a Windows icon.</a>
</p>
</li>

</ul>


        <div class="clearme" style='padding-top: 30px;'></div>

        <a name="wallpaper"></a>
        <h2>Misc Images and Wallpaper</h2>


<p>PlaysItAll made by <a href="http://www.xtremestuff.net/">Asim Siddiqui</a>: </p>
<img src="//images.videolan.org/images/goodies/vlc_playsitall.png"><img src="//images.videolan.org/images/goodies/cone_blue_strip_bluetriangle.png">

<p>Cone Icons with blue strip made by <a href="http://www.xtremestuff.net/">Asim Siddiqui</a>: </p>

<img src="//images.videolan.org/images/goodies/cone_blue_strip_orangeround.png"><img src="//images.videolan.org/images/goodies/cone_blue_strip.png">

<img src="//images.videolan.org/images/goodies/cone_blue_strip-audio64.png"><img src="//images.videolan.org/images/goodies/cone_blue_strip-video64.png"><img src="//images.videolan.org/images/goodies/cone_blue_strip-dvd64.png">

<h3>Funny cones for VLC preferences</h3>
<p>High-Quality Cone icons done by Tom Bigelajzen</p>
<p><a href="http://images.videolan.org/images/goodies/Cone-Shortcuts-large.png"><img src="//images.videolan.org/images/goodies/Cone-Shortcuts-small.png"></a>
<a href="http://images.videolan.org/images/goodies/Cone-Subs-large.png"><img src="//images.videolan.org/images/goodies/Cone-Subs-small.png"></a>
<a href="http://images.videolan.org/images/goodies/Cone-Forum-large.png"><img src="//images.videolan.org/images/goodies/Cone-Forum-small.png"></a>
<a href="http://images.videolan.org/images/goodies/Cone-Video-large.png"><img src="//images.videolan.org/images/goodies/Cone-Video-small.png"></a>
</p>
<p>
<a href="http://images.videolan.org/images/goodies/Cone-Audio-large.png"><img src="//images.videolan.org/images/goodies/Cone-Audio-small.png"></a>
<a href="http://images.videolan.org/images/goodies/Cone-Input-large.png"><img src="//images.videolan.org/images/goodies/Cone-Input-small.png"></a>
<a href="http://images.videolan.org/images/goodies/Cone-Interface-large.png"><img src="//images.videolan.org/images/goodies/Cone-Interface-small.png"></a>
<a href="http://images.videolan.org/images/goodies/Cone-List-large.png"><img src="//images.videolan.org/images/goodies/Cone-List-small.png"></a>
</p>

<p>Transparent cone by Yvo Schaap</p>
<p>
<a href="http://images.videolan.org/images/goodies/cone-icon-transparent.png"><img src="//images.videolan.org/images/goodies/cone-icon-transparent.png" width="200"></a>
</p>

<p>All videolan background images can now be downloaded in one
RPM package. The (relocatable) package has been built by <a href="http://www.jpsaman.org/">Jean-Paul Saman</a> on RH 9.0,
but should also be useable on SuSE, Mandrake and other RPM-aware
distributions. </p>

<p>To install the package:</p>

<ul class="bullets">
<li>Download the rpm here: <a href="http://images.videolan.org/images/videolan-backgrounds-base-2-1.noarch.rpm">videolan-backgrounds-base-2-1.noarch.rpm</a>.</li>

<li>Use "rpm ivh -prefix=&lt;newpath&gt;
videolan-backgrounds-base-2-1.noarch.rpm" to relocated the installed
images to a new location on e.g: SuSE systems.</li>
</ul>
<p>The source package can be found <a href="http://images.videolan.org/images/videolan-backgrounds-base-2-1.src.rpm">here</a>.</p>

<hr>

<p>Jason Jaeger made this VLC background:</p>

<div>
  <img src="//images.videolan.org/images/goodies/v3-wallpaper_300x240.jpg" alt="Jason Jaeger" height="240" width="300"><br>
  <a href="http://images.videolan.org/images/goodies/v3-wallpaper_1280x1024.jpg">Image 1280x1024</a> (163kB)<br>
  <a href="http://images.videolan.org/images/goodies/v3-wallpaper_1024x768.jpg">Image 1024x768</a> (167kB)<br>
  <a href="http://images.videolan.org/images/goodies/v3-wallpaper_800x600.jpg">Image 800x600</a> (121kB)
</div>


<p>HPep made this VLC background:</p>
<div>
  <img src="//images.videolan.org/images/goodies/vlc-hpep-300x225.jpg" alt="hpep" height="225" width="300"><br>
  <a href="http://images.videolan.org/images/goodies/vlc-hpep-1024x768.jpg">Image 1024x768</a> (159 kB)
</div>

<p>Emmanuel Puig (alias Karibu) made these VideoLAN logos:</p>
<div style="float:both;">
  <img src="//images.videolan.org/images/goodies/videolan/vl1_250x188.jpg" alt="Logo 1" height="188" width="250"><br>
    <a href="http://images.videolan.org/images/goodies/videolan/vl1_1600x1200.jpg">Image 1600x1200</a> (136 kB)<br>
    <a href="http://images.videolan.org/images/goodies/videolan/vl1_1024x768.jpg">Image 1024x768</a> (85 kB)
</div>
<div style="float:left;">
  <img src="//images.videolan.org/images/goodies/videolan/vl2_250x177.jpg" alt="Logo 2" height="177" width="250">
<br><a href="http://images.videolan.org/images/goodies/videolan/vl2_1600x1131.jpg">Image 1600x1131</a> (106 kB)<br>
<a href="http://images.videolan.org/images/goodies/videolan/vl2_1024x724.jpg">Image 1024x724</a> (67 kB)
</div>
<div>
  <img src="//images.videolan.org/images/goodies/videolan/vl3_250x188.jpg" alt="Logo 3" height="188" width="250">
<br><a href="http://images.videolan.org/images/goodies/videolan/vl3_1600x1200.jpg">Image 1600x1200</a> (75 kB)<br>
<a href="http://images.videolan.org/images/goodies/videolan/vl3_1024x768.jpg">Image 1024x768</a> (47 kB)
</div>

<p>Simon Latapie (alias Garf) made this VideoLAN logo:</p>
<div>
  <img src="//images.videolan.org/images/goodies/videolan/vl4_251x188.jpg" alt="Logo 4" height="188" width="251">
<br>
<a href="http://images.videolan.org/images/goodies/videolan/vl4_2872x2154.jpg">Image 2872x2154</a> (379 kB)<br>
<a href="http://images.videolan.org/images/goodies/videolan/vl4_1600x1200.jpg">Image 1600x1200</a> (99 kB)<br>
<a href="http://images.videolan.org/images/goodies/videolan/vl4_1024x768.jpg">Image 1024x768</a> (55 kB)
</div>

<p>This nice high-quality image was done by Tom Bigelajzen</p>
<p>
<a href="http://images.videolan.org/images/goodies/day-of-the-cones-ex2.jpg"><img src="//images.videolan.org/images/goodies/day-of-the-cones-small.jpg"></a>
</p>

<p>These nice logos where drawn by BRi7X:</p>
<p>
<a href="http://images.videolan.org/images/goodies/videolan-bri7x-6.png"><img src="//images.videolan.org/images/goodies/tn_videolan-bri7x-6.png" alt="Yet another VideoLAN logo" height="89" width="130"></a>
<a href="http://images.videolan.org/images/goodies/videolan-bri7x-5.png"><img src="//images.videolan.org/images/goodies/tn_videolan-bri7x-5.png" alt="Yet another VideoLAN logo" height="89" width="130"></a>
<a href="http://images.videolan.org/images/goodies/videolan-bri7x-4.png"><img src="//images.videolan.org/images/goodies/tn_videolan-bri7x-4.png" alt="Yet another VideoLAN logo" height="89" width="130"></a>
<a href="http://images.videolan.org/images/goodies/videolan-bri7x-3.png"><img src="//images.videolan.org/images/goodies/tn_videolan-bri7x-3.png" alt="Yet another VideoLAN logo" height="89" width="130"></a>
<a href="http://images.videolan.org/images/goodies/videolan-bri7x-2.png"><img src="//images.videolan.org/images/goodies/tn_videolan-bri7x-2.png" alt="Yet another VideoLAN logo" height="89" width="130"></a>
<a href="http://images.videolan.org/images/goodies/videolan-bri7x-1.png"><img src="//images.videolan.org/images/goodies/tn_videolan-bri7x-1.png" alt="Yet another VideoLAN logo" height="89" width="130"></a>
</p>


<p>These nice background images were made by <a href="mailto:graphics@wikingsoft.net">Peter Bauer</a> from Wikingsoft:</p>

<div>
  <img src="//images.videolan.org/images/goodies/videolan-wikingsoft.png" alt="Background" height="188" width="251"><br>
<a href="http://images.videolan.org/images/goodies/VideoLAN.png">Image 640x480</a> (188 kB)
</div>
<div>
  <img src="//images.videolan.org/images/goodies/videolan-wikingsoft2.png" alt="Background" height="188" width="251"><br>
<a href="http://images.videolan.org/images/goodies/videolan-wikingsoft2.png">Image 1024x768</a> (56 kB)
</div>

<p>This background image was made by <a href="http://www.xtremestuff.net/">Asim Siddiqui</a>:</p>

<div>
  <img src="//images.videolan.org/images/goodies/tn_videolan_org.png" alt="Background" height="187" width="250"><br>
<a href="http://images.videolan.org/images/goodies/videolan_org_1024x768.png">Image 1024x768</a> (15 kB)<br><br>
<a href="http://images.videolan.org/images/goodies/videolan_org_1280x960.png">Image 1280x960</a> (19 kB)<br><br>
<a href="http://images.videolan.org/images/goodies/videolan_org_1600x1200.png">Image 1600x1200</a> (26 kB)
</div>

<p></p>


        <div class="clearme" style='padding-top: 30px;'></div>

        <a name="boot"></a>
        <h2>Boot Screens</h2>


<p>Karibu also made a VideoLAN LILO bootscreen:</p>

<p><a href="http://images.videolan.org/images/goodies/lilo-videolan.jpg">
<img src="//images.videolan.org/images/goodies/lilo-videolan-small.jpg" alt="LILO bootscreen" height="300" width="401"></a></p>

<p>Download the image and the instructions: <a href="http://images.videolan.org/images/goodies/lilo-videolan.tar.gz">lilo-videolan.tar.gz</a>.</p>


</div>

        <div class="clearme" style='padding-top: 30px;'></div>

        <a name="banners"></a>
        <h2>Banners</h2>



<?php panel_start("blue" ); ?>

<p class="center"><a href="http://www.videolan.org/vlc" title="Get VLC media player - It plays, it streams, it kills WiMPs!!">
<img src="//images.videolan.org/images/goodies/buttons_getVLC/GetVLC_110.png" alt="Get VLC media player" height="45" width="110"></a></p>
<p>
Use the following code to include this banner in your site:</p>
<pre>&lt;a href="http://www.videolan.org/vlc"
 title="Get VLC media player -
        It plays, it streams, it kills WiMPs!!"&gt;
&lt;img
 src="http://www.videolan.org/images/
goodies/buttons_getVLC/GetVLC_110.png" width="110"
height="45" alt="Get VLC media player" /&gt;&lt;/a&gt;
</pre>
<p>
The following sizes are available:</p>
<ul class="bullets">
<li>
<a href="http://images.videolan.org/images/goodies/buttons_getVLC/GetVLC_110.png">Small banner</a></li>
<li>
<a href="http://images.videolan.org/images/goodies/buttons_getVLC/GetVLC_120.png">Medium banner</a></li>
<li><a href="http://images.videolan.org/images/goodies/buttons_getVLC/GetVLC_150.png">Large banner</a></li>
<li><a href="http://images.videolan.org/images/goodies/buttons_getVLC/GetVLC.png">Supersized banner</a></li>
<li><a href="http://images.videolan.org/images/goodies/buttons_getVLC/GetVLC.psd">Original Photoshop document</a></li>
</ul>

<p>This banner was created by
<a href="http://www.sidequest.org/">Derk-Jan Hartman</a>
and licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/2.0/">this Creative Commons License</a>.
</p>

<p>Another banner made by Gen:</p>
<p><a href="http://www.videolan.org/vlc/index.html"><img src="//images.videolan.org/images/goodies/getvlc.png" class="nofix" style="vertical-align: middle; border-width: 0px;" alt="get VLC !" height="15" width="80"></a>
</p>

<p>And another one by snis:</p>
<p><img src="//images.videolan.org/images/goodies/videolan-community.png" alt="VideoLAN.org Community Member"><br><img src="//images.videolan.org/images/goodies/videolan-community-small.png" alt="VideoLAN.org Community Member"></p>
<p>Get <a href="http://images.videolan.org/images/goodies/videolan-community.xcf">the sources</a>.</p>

<p>An older banner:</p>
<p><a href="http://www.videolan.org/vlc/index.html"><img src="//images.videolan.org/images/goodies/getvlcnow.png" style="vertical-align: middle; border-width: 0px;" alt="get VLC NOW!" height="32" width="100"></a>
</p>

<?php panel_end(); ?>


        <div class="clearme" style='padding-top: 30px;'></div>

        <a name="clothing"></a>
        <h2>Clothing</h2>

        <p>Soon !</p>




        <div class="clearme" style='padding-top: 30px;'></div>

        <a name="food"></a>
        <h2>Food</h2>

            <h3>VLC Chocolate bars</h3>
            <p>Get a hand on the famous and unique <a href="http://shop.borgodoro.it/products/vlc-chocolate">VLC Chocolate</a> made in Italia ! (from 30€/25 bars)</p>

            <img width="250" height="250" src="http://cdn.shopify.com/s/files/1/0293/4893/products/borgodoro-box_1024x1024.png?v=1384180629" alt="VLC Chocolate">




        <div class="clearme" style='padding-top: 30px;'></div>

        <a name="stationery"></a>
        <h2>Stationeries</h2>

            <h3>VLC Stickers and Bumpers</h3>

            <p>Stickers designed to customize your notebook, pc, workstation, server with a professional look and a fresh design, at <a href="http://www.unixstickers.com/vlc-videolan-software-shaped-sticker">UnixStickers</a></p>
            <img width="180" height="180" src="http://www.unixstickers.com/image/cache/data/stickers/vlc/vlc_bumper.sh-180x180.png" alt="VLC Stickers">


            <p>Another set of stickers is provided by <a href="http://www.it2l.com/videolan-vlc-d-31.html">it2l</a> (shipping only to France)</p>

<?php footer('$Id: goodies.php  4462 2008-02-23 13:20 altglass$'); ?>
