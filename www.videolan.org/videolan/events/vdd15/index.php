<?php
   $title = "VideoLAN Dev Days 2015, September 19 - 20, 2015";
   $additional_css = array("/style/panels.css");
   $body_color = "green";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<p><?php image( "events/vdd14.jpg" , "VDD14 Group picture"); ?></p>

<h1>VideoLAN Dev Days 2015</h1>
<h3 style="color: #4D4D4D">The Open Multimedia Conference that frees the cone in you!</h3>

<div id="sidebar">
<?php panel_start( "green" ); ?>
<h2>Sponsors</h2>
<a href="http://www.google.com/"><?php image( "partners/google.png" , "Google", "", 180); ?></a>
<br />
<a href="/videolan/"><?php image( "logoGrey.png" , "VideoLAN association"); ?></a>
<br />
<a href="http://videolabs.io"><?php image( "partners/videolabs-transparent.png" , "Videolabs SAS", "", 180); ?></a>
<h2>Host</h2>
<a href="http://www.criteo.fr/"><?php image( "partners/criteo.png" , "Criteo", "", 180); ?></a>
<?php panel_end(); ?>

<?php panel_start( "blue" ); ?>
<h2>Register!</h2>
<b><a href="http://goo.gl/forms/Kdi5LSu1u9">Register <strong>now!</strong></a></b>
<?php panel_end(); ?>

</div>

<p style="margin: 10px 0;"><b><br/>The <a href="/videolan/">VideoLAN non-profit organisation</a> is happy to
invite you to the multimedia open-source event of the summer!</b></p>

<p>For its <b>seventh edition</b>, people from the VideoLAN and open source multimedia communities will meet on the</p>
<p style="text-align: center"><strong>19th and 20th of September 2015</strong>,</p>
<p>to gather, work, discuss and meet, in:</p>
<p style="text-align: center"><strong>Paris, France</strong>.</p>
<p>This event is focused on technical discussions and decisions about <b>multimedia</b>.</p>
<p>Like in previous years, an important number of developers from <a href="/developers/x264.html">x264</a>, <a href="http://www.ffmpeg.org/">FFmpeg</a>, <a href="http://libav.org/">Libav</a> and <a href="http://kde.org/">KDE</a> will join us as well as major industry partners!
Developers, designers and anonymous people around <a href="/vlc/">VLC</a>, all of its ports to desktop, web browser environments and mobile operating systems, <a href="/projects/dvblast.html">DVBlast</a> or other multimedia projects will be there too.</p>

<h1>Where? </h1>
<p>The venue is in Paris, 9th district, near St Lazare.<br />
Criteo, 32 rue Blanche, 75009 Paris.</p>

<h1>Schedule</h1>
<div id="schedule" style="margin: 10px 25px 0px; padding: 0 20px; border: 0px outset #4d4d4d; width: 600px;">
<p>Download a printable copy <a href="http://images.videolan.org/images/events/VDD15.pdf">here</a></p>

<h2>Friday 18th September, Community Bonding Day</h2>
<p>This year we'll do a community bonding day at <a href="http://www.disneylandparis.com">Disneyland Paris</a>!<br/>
The VideoLAN organization will pay for the amusement park tickets.<br/>
<b>To participate you'll need to be in Paris Friday at 9h00!</b><br/>Please plan an extra night if you can't make it.</p>

<h2>Friday 18th September, Café Event</h2>
<p>On <strong>Friday 18th September 2015 at 20h30</strong>, people are welcome to come and
share a few good drinks. The location will be announced shortly.</p>

<h2>Saturday</h2>
<p>
<table style="width:100%">
  <tr>
    <td style="width:20%; color: #4D4D4D"><b>08:45 - 09:30</b></td>
    <td style="color: #4D4D4D">Registration and Breakfast</td>
  </tr>
  <tr>
    <td style="width:20%">&#8193;</td>
    <td>&#8193; </td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>09:30 - 09:45</b></td>
    <td style="color: #4D4D4D"><b>Opening remarks</b> by Jean-Baptiste Kempf <i>(VideoLAN)</i></td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>09:45 - 10:20</b></td>
    <td style="color: #4D4D4D"><b>Daala</b> by Timothy Terriberry <i>(Mozilla)</i></td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>10:20 - 10:55</b></td>
    <td style="color: #4D4D4D"><b>A new AAC free and open-source encoder</b> by Rostislav Pehlivanov <i>(FFmpeg)</i></td>
  </tr>
  <tr>
    <td style="width:20%">&#8193;</td>
    <td>&#8193;</td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>11:05 - 11:40</b></td>
    <td style="color: #4D4D4D"><b>New video compression techniques under consideration for VP10</b> by Alex Converse <i>(Google)</i></td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>11:40 - 12:10</b></td>
    <td style="color: #4D4D4D"><b>An update to x265</b> by Deepthi Nandakumar <i>(MCW)</i></td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>12:10 - 12:40</b></td>
    <td style="color: #4D4D4D"><b>VLC 3.0</b> by Jean-Baptiste Kempf <i>(VideoLAN)</i></td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>12:40 - 12:45</b></td>
    <td style="color: #4D4D4D"><b>Spice</b> by Lord Julien Navas <i>(Vodkaster)</i></td>
  </tr>
  <tr>
    <td style="width:20%">&#8193;</td>
    <td>&#8193; </td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>14:00 - 14:40</b></td>
    <td style="color: #4D4D4D"><b>The Thor Codec</b> by Thomas Davies <i>(Cisco)</i></td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>14:40 - 15:00</b></td>
    <td style="color: #4D4D4D"><b>How to optimize press coverage for FLOSS projects</b> by Sebastian Grüner <i>(golem.de)</i></td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>15:00 - 15:20</b></td>
    <td style="color: #4D4D4D"><b>A faster VP9 decoder</b> by Ronald Bultje</td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D; height:30px"><b>15:30 - 18:00</b></td>
    <td style="color: #4D4D4D"><b>Unconference</b></td>
  </tr>
</table>
</p>

<h2>Sunday</h2>
<p>
<table style="width:100%">
  <tr>
    <td style="width:20%; color: #4D4D4D"><b>09:30 - 10:00</b></td>
    <td style="color: #4D4D4D">Breakfast</td>
  </tr>
  <tr>
    <td style="width:20%">&#8193;</td>
    <td>&#8193; </td>
  </tr>
  <tr>
    <td style="width:20%; color: #4D4D4D"><b>10:00 - 18:00</b></td>
    <td style="color: #4D4D4D"><b>Unconference</b></td>
  </tr>
</table>
</p>
<p>The conference schedule is subject to changes. For live updates, check the <a href="https://wiki.videolan.org/VDD15">designated page on the wiki</a>.</p>
</div>

<h1>Who can come? </h1>
<p><strong>Anyone</strong> who cares about open source multimedia technologies and development. Remember that it targets a technical crowd!</p>
<p>If you are representing a <b>company</b> caring about open-source multimedia software, we would be <b>very interested</b> if you could co-sponsor the event.</p>

<h1>Cost and sponsorship </h1>
<p>The cost for attendance is <b>free</b>.</p>
<p>Like previous years, active developers can get a full sponsorship covering travel costs. We will also provide accomodation.</p>

<h1>Registration</h1>
<p>Please fill out the <b><a href="http://goo.gl/forms/Kdi5LSu1u9">registration form</a></b>.

<h1>Accommodation</h1>
<p>For active members of the open-source communities who registered until August 18, we provide accommodation at the <a href="http://www.timhotel.com/en/our-hotels-details/21-timhotel-berthier-paris-17-3.htm#tab-presentation">Timhotel Berthier Paris 17</a> near Porte de Clichy.<br />
4, boulevard Berthier, 75017 Paris.</p>

<h1><a name="coc">Code of Conduct</a> </h1>
<p>This community activity is running under the <a href="https://wiki.videolan.org/CoC/">VideoLAN Code of Conduct</a>. We expect all attendees to respect our <a href="https://wiki.videolan.org/VideoLAN_Values/">Shared Values</a>.</p>

<h1><a name="contact">Contact</a> </h1>
<p>The VideoLAN Dev Days are organized by the board members of the VideoLAN non-profit organization, Jean-Baptiste Kempf, Felix Paul Kühne and Ludovic Fauvet. You can reach us at <span style="color: #39b549">board@videolan.org</span>.</p>

<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>
